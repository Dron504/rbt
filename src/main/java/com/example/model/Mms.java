package com.example.model;

/**
 * Created by andrey on 26.04.16.
 */
public class Mms {

    private String msisdn;
    private Object rbt;

    public Mms() {
    }

    public Mms(String msisdn, Object rbt) {

        this.msisdn = msisdn;
        this.rbt = rbt;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public Object getRbt() {
        return rbt;
    }

    public void setRbt(Object rbt) {
        this.rbt = rbt;
    }
}

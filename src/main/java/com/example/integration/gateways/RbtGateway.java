package com.example.integration.gateways;

import com.example.model.Rbt;
import org.springframework.integration.annotation.Gateway;


public interface RbtGateway {

    @Gateway(replyChannel = "rbtChannel")
    Rbt getRbt(int id);

}
